// Copyright 2024 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/rcp.hpp"
#include "gtest/gtest.h"

#include <array>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
TEST(RCP, Polynomials) {
  {
    auto flt0 = std::array<float, 0>{};
    EXPECT_EQ(rcp::eval_polynomial(flt0, 8.0F), 0.0F);
    EXPECT_EQ(rcp::eval_polynomial((std::array<float, 0>{}), 88.0F), 0.0F);
  }
  {
    auto flt1 = std::array{1.0F};
    EXPECT_EQ(rcp::eval_polynomial(flt1, 3.0F), flt1[0]);
    EXPECT_EQ(rcp::eval_polynomial({10.0F}, -2.0F), 10.0F);
  }
  {
    auto flt2 = std::array{1.0F, -3.5F};
    auto poly = [&](auto val) { return flt2[0] + flt2[1] * val; };
    EXPECT_EQ(rcp::eval_polynomial(flt2, 3.0F), poly(3.0F));
    EXPECT_EQ(rcp::eval_polynomial({flt2[0], flt2[1]}, -2.0F), poly(-2.0F));
  }
  {
    auto flt3 = std::array{1.0F, -3.5F, 0.6F};
    auto poly = [&](auto val) {
      return flt3[0] + val * (flt3[1] + val * flt3[2]);
    };
    EXPECT_EQ(rcp::eval_polynomial(flt3, 3.0F), poly(3.0F));
    EXPECT_EQ(
      rcp::eval_polynomial({flt3[0], flt3[1], flt3[2]}, -2.0F), poly(-2.0F));
  }
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
