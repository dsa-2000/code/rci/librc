# librcp

Common functions and class definitions for `rcp`. A significant part
of this library is to include `std::experimental::mdspan`, using the
implementation from Kokkos (but not dependent on Kokkos). Also
includes some utility functions and a `bark` logging interface.

## Build guidance

- Minimum [CMake](https://cmake.org) version: 3.23
- C++ standard: 20
  - tested with [gcc](https://gcc.gnu.org) v12.1.0
- Dependencies
  - [bark](https://gitlab.com/dsa-2000/rcp/bark), minimum
    version 0.103.0
  - [Legion](https://legion.stanford.edu)
  - [Kokkos](https://kokkos.org), optional, minimum version 4.0.1
  - [CUDA](https://developer.nvidia.com/cuda-toolkit), optional,
    minimum version 12

## Spack package

Spack package is available in the DSA-2000 Spack [package
repository](https://gitlab.com/dsa-2000/code/spack).

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/rcp/librcp. Instructions for cloning the
code repository can be found on that site. To provide feedback (*e.g*
bug reports or feature requests), please see the issue tracker on that
site. If you wish to contribute to this project, please read
[CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.
