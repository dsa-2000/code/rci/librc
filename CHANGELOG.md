## 0.108.0
 (2024-12-10)

### New features (2 changes)

- [Add more constructors for RegionPair and PartitionPair](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/6066c980c215d1f64945a70f216af251920e73d4)
- [Add serialization support to DenseArrayRegionGenerator](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/4af6b969db45f6026ad9f0f1c0d638e1bcf0d59d)

### Bug fixes (3 changes)

- [Fix block_distribute() for small "items" values.](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/366230b4801baed2754cad2ba22f0edd639ee248)
- [Fix return statements for body() in task implementation mixin classes](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/468393d6bf9f092468be319bf9bff41ff8ac3d79)
- [Maintain "registrar" name strings on heap](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/91f156827a40f72e5dd72b65514cbc71462f031a)

### Feature changes (1 change)

- [Update mapper](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/e25d4048b45ddbd94048bf0f5b03fa33392d5322)

## 0.107.0
 (2024-11-05)

### New features (4 changes)

- [Add PortableLayout](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/51492cabf3c1526fb093d46829a207b2dfe21a5e)
- [Add mixin classes for Kokkos and serial-only tasks](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/2a09a6e9949d2abf1c8d3d56c35288f10a13474a)
- [Add PortableTask](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/7caf9bccf268df019de624c4c0fd1f12117a9a96)
- [Add DataPacketConfiguration::volume](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/eca0d746c32d553ef369c71f9311a23ac21e21d0)

### Bug fixes (1 change)

- [Fix conditionals used to set RCP_USE_* macro values](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/3f531eeeeb4b82fe866c3adaa3eea5bfab1bb376)

### Feature changes (1 change)

- [Replace flagged_sample value with is_flagged() function](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/f897a707f717914c0bc117eeecea79d3d1062887)

## 0.106.0
 (2024-09-23)

### New features (2 changes)

- [Add configuration variables for some common fp types](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/bcdd6de65d5e98faac3218169ce945609fe9a686)
- [Add dim() and range() (with enum axis value)](https://gitlab.com/dsa-2000/rcp/librcp/-/commit/28f883d905919bf23b55a02768ce0c7335bfa070)

## v0.105.0
 (2024-09-06)

### New features

- Merged with library `librcpl`.

## v0.104.0
 (2024-01-30)

### New features (1 change)

- [Add serialization/deserialization functions for std::set types](dsa-2000/rcp/librcp@286619b086bd0d3a645e38aa7002a11ed0c126c5)

### Feature changes (1 change)

- [Simplified type declarations in receiver_pair_to_baseline()](dsa-2000/rcp/librcp@772882e7a4cc4df8279ad1f2e0087f9fc3f9b4b3)

## v0.103.0
 (2023-11-21)

### Other (1 change)

- [Update bark dependency to version 0.103.0](dsa-2000/rcp/librcp@a4d92badceda38eab91617ca226b4587c29c0a91)

## v0.102.0
 (2023-06-30)

### Feature changes (1 change)

- [Capitalize CMake target namespaces](dsa-2000/code/rcp/librcp@2fe588b1a87b45880f6af1e1395b72f9f6656217)

### Other (1 change)

- [Update bark dependency version to v0.102.0](dsa-2000/code/rcp/librcp@c23c234a2292de2e28d945007ab95fdd493a3b3b)

## v0.101.0
 (2023-06-12)

### Bug fixes (1 change)

- [Add mdspan sub-directory header files to installation](dsa-2000/code/rcp/librcp@7da5b7b1c17d26321c9a933522114f065ec6ab15)

## v0.100.0
 (2023-06-11)

### Other (1 change)

- Set version to 0.100.0
