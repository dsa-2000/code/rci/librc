// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcp.hpp"

#include <chrono>
#include <limits>
#include <ostream>

namespace rcp {

struct RCP_EXPORT DataPacketConfiguration {

  using receiver_type = std::uint16_t;
  using timestamp_type = std::int64_t;
  using timestep_offset_type = std::uint32_t;
  using channel_type = std::uint16_t;
  using ch_offset_type = std::uint8_t;
  using polarization_type = std::uint8_t;
  using sequence_type = timestep_offset_type;
  struct fsample_type {
    std::int8_t i{}; /* TODO: change this? */
  };
  using fweight_type = std::uint8_t;
  struct fvalue_type {
    fsample_type sample{};
    fweight_type weight{};
  };

  /*! number of channels per F-engine packet */
  static constexpr unsigned num_channels_per_packet =
    RCP_NUM_CHANNELS_PER_PACKET;

  /*! number of timesteps per F-engine packet */
  static constexpr unsigned num_timesteps_per_packet =
    RCP_NUM_TIMESTEPS_PER_PACKET;

  /*! timesteps tile size
   *
   * value of 0 corresponds to no tiling
   */
  static constexpr unsigned timesteps_tile_size =
    RCP_PACKET_TIMESTEPS_TILE_SIZE;

  static_assert(
    timesteps_tile_size == 0
    || num_timesteps_per_packet % timesteps_tile_size == 0);

  /*! number of polarizations per channel from F-engine */
  static constexpr unsigned num_polarizations = RCP_NUM_POLARIZATIONS;

  /*! number of receivers (antennas) from F-engines */
  static constexpr unsigned num_receivers = RCP_NUM_RECEIVERS;

  static constexpr auto volume =
    num_polarizations * num_channels_per_packet * num_timesteps_per_packet;

  /*! duration type for a fsample
   */
  using fsample_duration_t = std::chrono::duration<
    std::int64_t,
    std::ratio_multiply<std::ratio<RCP_SAMPLE_INTERVAL_NS, 1>, std::nano>>;
  static constexpr fsample_duration_t fsample_interval{1};

  static RCP_INLINE_FUNCTION auto
  is_flagged(fsample_type val) -> bool {

    using rep = std::make_unsigned_t<decltype(val.i)>;
    // minimum signed integer bit pattern for real or complex part
    rep constexpr part_min_signed = (1 << (4 * sizeof(rep) - 1));
    // invalid value of imaginary part
    rep constexpr invalid_imag = part_min_signed << (4 * sizeof(rep));
    // bit mask for imaginary part
    rep constexpr imag_part =
      std::numeric_limits<rep>::max()
      ^ (rep(1 << (std::numeric_limits<rep>::digits / 2)) - 1);
    return (val.i & imag_part) != invalid_imag;
  }
};

RCP_EXPORT auto
operator<<(std::ostream&, rcp::DataPacketConfiguration const&) -> std::ostream&;

} // end namespace rcp

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
