// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/rcp.hpp"

#include <cstring>

using namespace rcp;

// NOLINTBEGIN(cppcoreguidelines-avoid-non-const-global-variables)
LogSink<Tag> rcp::log_rcp;
// NOLINTEND(cppcoreguidelines-avoid-non-const-global-variables)

template <>
auto
rcp::get_log_sink<Tag>() -> LogSink<Tag>& {
  return log_rcp;
}

auto
rcp::serialized_size(std::string const& val) -> std::size_t {
  return sizeof(std::size_t) + val.size();
}

auto
rcp::serialize(std::string const& str, void* buffer) -> std::size_t {

  // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
  char* bf = reinterpret_cast<char*>(buffer);
  auto sz = str.size();
  bf += serialize(sz, bf);
  std::memcpy(bf, str.c_str(), sz);
  bf += sz;
  return bf - reinterpret_cast<char*>(buffer);
  // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
}

auto
rcp::deserialize(std::string& str, void const* buffer) -> std::size_t {
  // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
  char const* bf = reinterpret_cast<char const*>(buffer);
  std::size_t sz = 0;
  bf += deserialize(sz, bf);
  new (&str) std::string;
  str.resize(sz);
  std::memcpy(str.data(), bf, sz);
  assert(str.data()[sz] == '\0');
  bf += sz;
  return bf - reinterpret_cast<char const*>(buffer);
  // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
